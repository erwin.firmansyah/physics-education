package com.example.physicseducation.model

class TheoryModel {

    data class Theory(
        var title : String = "",
        var videoTitle : String = "",
        var description : String = ""
    )

}