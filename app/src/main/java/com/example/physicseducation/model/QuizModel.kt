package com.example.physicseducation.model

class QuizModel {

    data class QuestionQuiz(
        var number : Int = 0,
        var question : String = "",
        var answerList : List<Answer> = listOf(),
        var review : Int = 0,
        var imageHeight : Int = 0,
        var answerIsTrue : Boolean = false,
        var userAnswer : Answer? = null
    )

    data class Answer(
        var position : String = "",
        var answerText : String = "",
        var isTrue : Boolean = false,
        var isSelected : Boolean = false,
    )
}