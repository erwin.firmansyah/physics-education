package com.example.physicseducation.extension

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.physicseducation.R

fun Fragment.addChildFragment(frameId: Int, fragment: Fragment) {
    childFragmentManager.inTransaction { add(frameId, fragment) }
}

fun Fragment.replaceChildFragment(containerViewId: Int, fragment: Fragment) {
    childFragmentManager.beginTransaction().replace(containerViewId, fragment).commitAllowingStateLoss()
}

fun Fragment.replaceParentFragment(containerViewId: Int, fragment: Fragment) {
    parentFragmentManager.beginTransaction().replace(containerViewId, fragment).addToBackStack("home").commit()
}

fun View.setViewVisibility(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun goToFragment(fm : FragmentManager, fragment : Fragment, bundle : Bundle?=null, tag : String?){
    val fragmentTransaction : FragmentTransaction = fm.beginTransaction()
    if(bundle != null){
        fragment.arguments = bundle
    }
    if(fragment.isAdded)
        return
    fragmentTransaction.replace(R.id.container_fragment, fragment, tag)
    fragmentTransaction.commit()
//        if(fm.findFragmentByTag(tag) == null && tag != null)
    fragmentTransaction.addToBackStack(tag)
}


fun Fragment.openAppBrowser(url: String) {
    try {
        var direct = url
        if (!url.startsWith("http://") && !url.startsWith("https://")) direct = "http://$url"
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(direct))
        requireContext().startActivity(browserIntent)
    } catch (e: Exception) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://play.google.com/store/apps/details?id=com.android.chrome")
        )
        startActivity(intent, null)
    }
}

private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}