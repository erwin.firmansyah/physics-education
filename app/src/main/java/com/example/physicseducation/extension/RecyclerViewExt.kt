package com.example.physicseducation.extension

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setupHorizontal(context: Context, recyclerAdapter: RecyclerView.Adapter<*>) {
    val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    apply {
        setHasFixedSize(true)
        layoutManager = linearLayoutManager
        adapter = recyclerAdapter
    }
}

fun RecyclerView.setupVertical(
    context: Context,
    recyclerAdapter: RecyclerView.Adapter<*>,
) {
    val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    apply {
        setHasFixedSize(true)
        layoutManager = linearLayoutManager
        adapter = recyclerAdapter
    }
}

fun getConditionTrue(): Boolean = true
