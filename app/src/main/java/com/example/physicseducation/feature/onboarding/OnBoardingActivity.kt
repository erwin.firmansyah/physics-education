package com.example.physicseducation.feature.onboarding

import androidx.fragment.app.Fragment
import com.example.physicseducation.base.BaseFullScreenActivity

class OnBoardingActivity : BaseFullScreenActivity() {

    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        return newInstanceFragment(bundle, OnBoardingFragment())
    }

}