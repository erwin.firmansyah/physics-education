package com.example.physicseducation.feature.video

import androidx.fragment.app.Fragment
import com.example.physicseducation.base.BaseFullScreenActivity

class VideoActivity : BaseFullScreenActivity() {

    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        return newInstanceFragment(bundle, VideoFragment())
    }
}