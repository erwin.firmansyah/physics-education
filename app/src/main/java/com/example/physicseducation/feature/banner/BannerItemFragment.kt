package com.example.physicseducation.feature.banner

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.physicseducation.MainActivity
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.base.EmptyViewModel
import com.example.physicseducation.base.ViewPagerAdapter
import com.example.physicseducation.databinding.FragmentBannerBinding
import com.example.physicseducation.databinding.FragmentOnboardingBinding
import com.google.android.material.tabs.TabLayoutMediator
import java.util.*
import kotlin.concurrent.schedule

class BannerItemFragment : BaseFragment<EmptyViewModel, FragmentBannerBinding>() {

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentBannerBinding =
        FragmentBannerBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewPagerV2()
    }

    private fun initViewPagerV2() {
        val fragments = ArrayList<Fragment>()
        fragments.clear()
        fragments.add(newInstanceFragment(Bundle(), BannerItem1Fragment()))
        fragments.add(newInstanceFragment(Bundle(), BannerItem2Fragment()))
        val pagerAdapter = ViewPagerAdapter(childFragmentManager, lifecycle, fragments)
        if (mViewDataBinding.viewPager.adapter == null) {
            mViewDataBinding.viewPager.adapter = pagerAdapter
            mViewDataBinding.viewPager.offscreenPageLimit = fragments.size
            TabLayoutMediator(
                mViewDataBinding.tabIndicator, mViewDataBinding.viewPager
            ) { tab, _ ->
                tab.view.isClickable = false
            }.attach()
        }
        mViewDataBinding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0 -> stateFirstView()
                    1 -> stateEndView()
                }
            }
        })
    }

    private fun stateFirstView() {

    }

    private fun stateEndView() {

    }
}