package com.example.physicseducation.feature.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.physicseducation.MainActivity
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.base.EmptyViewModel
import com.example.physicseducation.databinding.FragmentSplashScreenBinding
import com.example.physicseducation.feature.onboarding.OnBoardingActivity
import com.example.physicseducation.util.PreferenceHelper

class SplashFragment : BaseFragment<EmptyViewModel, FragmentSplashScreenBinding>() {

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSplashScreenBinding =
        FragmentSplashScreenBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pref = PreferenceHelper(requireContext())
        Handler(Looper.getMainLooper()).postDelayed({
            if(pref.isFirstInstall()){
                startNavigationTo(OnBoardingActivity::class.java, Bundle())
            }else{
                startNavigationTo(MainActivity::class.java, Bundle())
            }
            activity?.finish()
        }, 2000)
    }
}