package com.example.physicseducation.feature.onboarding

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.physicseducation.MainActivity
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.base.EmptyViewModel
import com.example.physicseducation.base.ViewPagerAdapter
import com.example.physicseducation.databinding.FragmentOnboardingBinding
import com.example.physicseducation.util.PreferenceHelper
import com.google.android.material.tabs.TabLayoutMediator
import java.util.*
import kotlin.concurrent.schedule

class OnBoardingFragment : BaseFragment<EmptyViewModel, FragmentOnboardingBinding>() {

    private val fadeAnimation = 200L
    private val floatAnimationIn = 1.0f
    private val floatAnimationOff = 0.0f

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnboardingBinding =
        FragmentOnboardingBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pref = PreferenceHelper(requireContext())
        initViewPagerV2()
        mViewDataBinding.btnGetStarted.setOnClickListener {
            pref.setFirstInstall(false)
            startNavigationTo(MainActivity::class.java, Bundle())
        }
    }

    private fun initViewPagerV2() {
        val fragments = ArrayList<Fragment>()
        fragments.clear()
        fragments.add(newInstanceFragment(Bundle(), OnBoardingItem1Fragment()))
        fragments.add(newInstanceFragment(Bundle(), OnBoardingItem2Fragment()))
        val pagerAdapter = ViewPagerAdapter(childFragmentManager, lifecycle, fragments)
        if (mViewDataBinding.viewPager.adapter == null) {
            mViewDataBinding.viewPager.adapter = pagerAdapter
            mViewDataBinding.viewPager.offscreenPageLimit = fragments.size
            TabLayoutMediator(
                mViewDataBinding.tabIndicator, mViewDataBinding.viewPager
            ) { tab, _ ->
                tab.view.isClickable = false
            }.attach()
        }
        mViewDataBinding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0 -> stateFirstView()
                    1 -> stateEndView()
                }
            }
        })
    }

    private fun stateFirstView() {
        mViewDataBinding.btnGetStarted.animate().alpha(floatAnimationOff).duration = fadeAnimation
        Handler(Looper.getMainLooper()).postDelayed({
            mViewDataBinding.btnGetStarted.visibility = View.GONE
        }, 500)
    }

    private fun stateEndView() {
        mViewDataBinding.btnGetStarted.animate().alpha(floatAnimationIn).duration = fadeAnimation
        mViewDataBinding.btnGetStarted.visibility = View.VISIBLE
    }
}