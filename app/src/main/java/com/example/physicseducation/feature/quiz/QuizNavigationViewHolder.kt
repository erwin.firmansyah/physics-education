package com.example.physicseducation.feature.quiz

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.physicseducation.R
import com.example.physicseducation.databinding.ItemCircleTextBinding
import com.example.physicseducation.extension.defaultZero
import com.example.physicseducation.model.QuizModel
import javax.inject.Inject

class QuizNavigationViewHolder@Inject constructor(private val dataBinding: ItemCircleTextBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(
        dataQuiz: QuizModel.QuestionQuiz,
        position: Int,
    ) {
        dataBinding.apply {
            tvPage.text = dataQuiz.number.defaultZero().toString()

            if(position+1 == dataQuiz.number){
                tvPage.setTextColor(ContextCompat.getColor(root.context, R.color.text_primary))
                tvPage.background = ContextCompat.getDrawable(root.context, R.drawable.bg_circle_white)
            }else{
                tvPage.setTextColor(ContextCompat.getColor(root.context, R.color.white))
                tvPage.background = ContextCompat.getDrawable(root.context, R.drawable.bg_circle_black)
            }
        }
    }
}