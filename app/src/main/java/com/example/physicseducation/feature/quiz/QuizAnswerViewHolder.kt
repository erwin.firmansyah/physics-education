package com.example.physicseducation.feature.quiz

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.physicseducation.R
import com.example.physicseducation.databinding.ItemAnswerSelectableBinding
import com.example.physicseducation.listener.OnAnswerClickListener
import com.example.physicseducation.model.QuizModel
import javax.inject.Inject

class QuizAnswerViewHolder@Inject constructor(private val dataBinding: ItemAnswerSelectableBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(
        answer: QuizModel.Answer,
        listener: OnAnswerClickListener? = null,
        isSelected: Boolean = false
    ) {
        dataBinding.apply {
            tvOption.text = answer.position
            tvAnswer.text = answer.answerText
            if(isSelected) {
                cvAnswer.setCardBackgroundColor(ContextCompat.getColor(root.context, R.color.primary_dark))
            }else{
                cvAnswer.setCardBackgroundColor(ContextCompat.getColor(root.context, R.color.primary_light))
            }

            cvAnswer.setOnClickListener {
                listener?.onClick(answer)
            }
        }
    }
}