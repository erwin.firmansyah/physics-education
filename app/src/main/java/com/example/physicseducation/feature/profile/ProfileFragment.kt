package com.example.physicseducation.feature.profile

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.lifecycle.ViewModelProvider
import coil.load
import coil.transform.CircleCropTransformation
import com.example.physicseducation.MainActivity
import com.example.physicseducation.R
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.databinding.FragmentHomeBinding
import com.example.physicseducation.databinding.FragmentProfileBinding
import com.example.physicseducation.extension.addChildFragment
import com.example.physicseducation.extension.defaultEmpty
import com.example.physicseducation.feature.banner.BannerItemFragment
import com.example.physicseducation.feature.onboarding.OnBoardingActivity
import com.example.physicseducation.util.PreferenceHelper
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import java.io.*
import java.sql.Date
import java.util.*
import kotlin.system.exitProcess

class ProfileFragment : BaseFragment<ProfileViewModel, FragmentProfileBinding>() {

    private var isWritePermissionGranted = false
    private var isReadPermissionGranted = false
    private var dataUriImage : Uri? = null
    private lateinit var permissionLauncher: ActivityResultLauncher<Array<String>>

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == Activity.RESULT_OK){
            if(it.data!=null){
                dataUriImage = it.data?.data
                if(dataUriImage!=null){
                    mViewDataBinding.ivPhotoProfile.load(dataUriImage){
                        transformations(CircleCropTransformation())
                        error(R.drawable.icon_default)
                    }
                }
            }
        }
    }

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(ProfileViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentProfileBinding =
        FragmentProfileBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requestPermission()

        pref = PreferenceHelper(requireContext())
        mViewDataBinding.etName.setText(pref.getProfileName())

        val photo = pref.getImageProfileUri()
        mViewDataBinding.ivPhotoProfile.load(photo.toUri()){
            transformations(CircleCropTransformation())
            error(R.drawable.icon_default)
        }

        mViewDataBinding.btnSave.setOnClickListener {
            (activity as MainActivity).customDialog.setupDialog(
                "Apakah anda ingin mengganti nama profil?",
                true,
                {
                    val name = mViewDataBinding.etName.text.toString()
                    pref.setProfileName(name)
                    pref.saveImageProfileUri(dataUriImage.toString())
                    Snackbar.make(mViewDataBinding.containerLayout, "Profil berhasil diperbarui", Snackbar.LENGTH_SHORT).show()
                    startNavigationTo(MainActivity::class.java, Bundle())
                    activity?.finish()
                },
                R.color.text_primary
            )
        }

        mViewDataBinding.btnChangePhoto.setOnClickListener {
            openGalleryForImage()
        }
    }

    private fun openGalleryForImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        resultLauncher.launch(intent)
    }

    private fun requestPermission(){
        permissionLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){
            isReadPermissionGranted = it[Manifest.permission.READ_EXTERNAL_STORAGE] ?: isReadPermissionGranted
            isWritePermissionGranted = it[Manifest.permission.WRITE_EXTERNAL_STORAGE] ?: isWritePermissionGranted
        }

        val permissionRequest = mutableListOf<String>()

        isReadPermissionGranted = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

        isWritePermissionGranted = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

        if(!isReadPermissionGranted) permissionRequest.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        if(!isWritePermissionGranted) permissionRequest.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if(permissionRequest.isNotEmpty()) permissionLauncher.launch(permissionRequest.toTypedArray())
    }
}