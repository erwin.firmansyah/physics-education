package com.example.physicseducation.feature.splash

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import com.example.physicseducation.base.BaseFullScreenActivity

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseFullScreenActivity() {

    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        return newInstanceFragment(bundle, SplashFragment())
    }

}