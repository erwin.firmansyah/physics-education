package com.example.physicseducation.feature.video

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.physicseducation.base.BaseViewModel

class VideoViewModel : BaseViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is video Fragment"
    }
    val text: LiveData<String> = _text
}