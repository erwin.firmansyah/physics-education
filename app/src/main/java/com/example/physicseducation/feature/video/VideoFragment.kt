package com.example.physicseducation.feature.video

import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import com.example.physicseducation.R
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.databinding.FragmentVideoBinding
import com.example.physicseducation.extension.defaultEmpty
import com.example.physicseducation.extension.defaultZero
import com.example.physicseducation.util.GlobalConstant
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.DefaultTimeBar
import com.google.android.exoplayer2.ui.PlayerView


class VideoFragment : BaseFragment<VideoViewModel, FragmentVideoBinding>(), Player.Listener {

    private var videoPath = ""
    private var theory = ""
    private var theoryDesc : Int = 0
    private var theoryChosing : Int = 0
    private var fullscreen = false
    private lateinit var exoPlayer : ExoPlayer
    private lateinit var playerView : PlayerView
    private lateinit var progressBar : DefaultTimeBar
    private lateinit var buttonFullScreen : ImageButton


    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(VideoViewModel::class.java)

        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_PATH_VIDEO)) {
            theoryChosing = requireArguments().getInt(GlobalConstant.Extras.EXTRAS_PATH_VIDEO).defaultZero()
        }
        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_THEORY_VIDEO)) {
            theory = requireArguments().getString(GlobalConstant.Extras.EXTRAS_THEORY_VIDEO).defaultEmpty()
        }

        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_THEORY_DESC)) {
            theoryDesc = requireArguments().getInt(GlobalConstant.Extras.EXTRAS_THEORY_DESC).defaultZero()
        }

    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentVideoBinding =
        FragmentVideoBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        videoPath = "android.resource://" + requireContext().packageName + "/" + theoryChosing
        val uri = Uri.parse(videoPath)

        setupPlayer()
        pickFile(uri)
        checkFullscreen()
        mViewDataBinding.apply {
            tvTheoryTitle.text = theory
            ivTheoryDesc.setImageResource(theoryDesc)
            ivArrowBack.setOnClickListener{
                (activity as VideoActivity).onBackPressed()
            }
        }
    }

    override fun onPlaybackStateChanged(playbackState: Int) {
        super.onPlaybackStateChanged(playbackState)
    }

    override fun onStop() {
        super.onStop()
        exoPlayer.release()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        exoPlayer.release()
    }

    private fun setupPlayer(){
        exoPlayer = ExoPlayer.Builder(requireContext()).build()
        val progressBarIdentifier = resources.getIdentifier("exo_progress", "id", context?.packageName)
        val btnFullscreen = resources.getIdentifier("btn_fullscreen", "id", context?.packageName)
        progressBar = mViewDataBinding.videoView.findViewById(progressBarIdentifier)
        buttonFullScreen = mViewDataBinding.videoView.findViewById(btnFullscreen)

        playerView = mViewDataBinding.videoView
        playerView.player = exoPlayer
        exoPlayer.addListener(this)

        buttonFullScreen.setOnClickListener {
            if(!fullscreen){
                (activity as VideoActivity).requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }else{
                (activity as VideoActivity).requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                mViewDataBinding.videoView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
                buttonFullScreen.setImageResource(R.drawable.ic_fullscreen)
                val relativeLayoutParam = RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, (220 * mViewDataBinding.root.context.resources.displayMetrics.density).toInt()
                )
                mViewDataBinding.frameVideo.layoutParams = relativeLayoutParam
                fullscreen = false
            }
        }
    }

    private fun pickFile(url : Uri){
        val mediaItem = MediaItem.fromUri(url)
        exoPlayer.addMediaItem(mediaItem)
        exoPlayer.prepare()
    }

    private fun checkFullscreen(){
        if((activity as VideoActivity).requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
            mViewDataBinding.videoView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
            buttonFullScreen.setImageResource(R.drawable.ic_fullscreen_exit)
            val relativeLayoutParam = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            mViewDataBinding.frameVideo.layoutParams = relativeLayoutParam
            fullscreen = true
        }
    }
}