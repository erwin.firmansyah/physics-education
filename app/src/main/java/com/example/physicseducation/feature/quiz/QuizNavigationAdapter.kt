package com.example.physicseducation.feature.quiz

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.physicseducation.R
import com.example.physicseducation.databinding.ItemCircleTextBinding
import com.example.physicseducation.model.QuizModel

class QuizNavigationAdapter : RecyclerView.Adapter<QuizNavigationViewHolder>() {

    private var data: List<QuizModel.QuestionQuiz> = arrayListOf()
    private var positionQuiz : Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizNavigationViewHolder {
        val viewBinding: ItemCircleTextBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_circle_text, parent, false)
        return QuizNavigationViewHolder(viewBinding)
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(holder: QuizNavigationViewHolder, position: Int) {
        holder.bind(data[position], positionQuiz)
    }

    fun updateData(data: List<QuizModel.QuestionQuiz>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun updatePositionQuizActive(position: Int){
        this.positionQuiz = position
    }
}