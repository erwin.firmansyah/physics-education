package com.example.physicseducation.feature.quiz

import androidx.fragment.app.Fragment
import com.example.physicseducation.base.BaseFullScreenActivity
import com.example.physicseducation.util.*

class QuizActivity : BaseFullScreenActivity() {

    var trueAnswerLottie : SuccessAnswerLottie = SuccessAnswerLottie(this)
    var wrongAnswerLottie : WrongAnswerLottie = WrongAnswerLottie(this)
    val customDialog = CustomDialog(this)
    var reviewDialog = ReviewDialog(this)
    var reviewQuizDialog = ReviewQuizDialog(this)


    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        return newInstanceFragment(bundle, QuizFragment())
    }
}