package com.example.physicseducation.feature.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.base.EmptyViewModel
import com.example.physicseducation.databinding.LayoutOnboarding2Binding

class OnBoardingItem2Fragment : BaseFragment<EmptyViewModel, LayoutOnboarding2Binding>() {


    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): LayoutOnboarding2Binding =
        LayoutOnboarding2Binding.inflate(layoutInflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun getMainContainer(): View? = null
}
