package com.example.physicseducation.feature.banner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.base.EmptyViewModel
import com.example.physicseducation.databinding.LayoutBannerItem1Binding
import com.example.physicseducation.databinding.LayoutBannerItem2Binding
import com.example.physicseducation.databinding.LayoutOnboarding1Binding
import com.example.physicseducation.extension.openAppBrowser

class BannerItem2Fragment : BaseFragment<EmptyViewModel, LayoutBannerItem2Binding>() {


    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): LayoutBannerItem2Binding =
        LayoutBannerItem2Binding.inflate(layoutInflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewDataBinding.ivBanner.setOnClickListener {
            openAppBrowser("https://phet.colorado.edu/en/simulations/collision-lab")
        }
    }

    override fun getMainContainer(): View? = null
}
