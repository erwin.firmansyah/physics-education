package com.example.physicseducation.feature.quiz

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.physicseducation.R
import com.example.physicseducation.databinding.ItemAnswerSelectableBinding
import com.example.physicseducation.databinding.ItemCircleTextBinding
import com.example.physicseducation.listener.OnAnswerClickListener
import com.example.physicseducation.model.QuizModel

class QuizAnswerAdapter : RecyclerView.Adapter<QuizAnswerViewHolder>() {

    private var data: List<QuizModel.Answer> = arrayListOf()
    private var userAnswer : QuizModel.Answer? = null
    private var listener : OnAnswerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizAnswerViewHolder {
        val viewBinding: ItemAnswerSelectableBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_answer_selectable, parent, false)
        return QuizAnswerViewHolder(viewBinding)
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(holder: QuizAnswerViewHolder, position: Int) {
        val isSelected = userAnswer == data[position]
        holder.bind(data[position], listener, isSelected)
    }

    fun updateData(data: QuizModel.QuestionQuiz) {
        this.data = data.answerList
        this.userAnswer = data.userAnswer
        notifyDataSetChanged()
    }

    fun setListener(listener: OnAnswerClickListener){
        this.listener = listener
    }
}