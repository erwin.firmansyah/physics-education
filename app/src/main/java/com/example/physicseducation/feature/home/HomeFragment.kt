package com.example.physicseducation.feature.home

import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import coil.load
import coil.transform.CircleCropTransformation
import com.example.physicseducation.MainActivity
import com.example.physicseducation.R
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.databinding.FragmentHomeBinding
import com.example.physicseducation.extension.addChildFragment
import com.example.physicseducation.extension.replaceChildFragment
import com.example.physicseducation.extension.replaceParentFragment
import com.example.physicseducation.feature.banner.BannerItemFragment
import com.example.physicseducation.feature.quiz.QuizActivity
import com.example.physicseducation.feature.video.VideoActivity
import com.example.physicseducation.feature.video.VideoFragment
import com.example.physicseducation.util.GlobalConstant
import com.example.physicseducation.util.PreferenceHelper
import com.google.android.material.appbar.AppBarLayout

class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>() {

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding =
        FragmentHomeBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        pref = PreferenceHelper(requireContext())
        val name = pref.getProfileName().ifEmpty {
            "There"
        }
        mViewDataBinding.tvName.text = name.split(" ")[0]
        val profile = pref.getImageProfileUri()

        mViewDataBinding.ivHomePhoto.load(profile.toUri()){
            transformations(CircleCropTransformation())
            error(R.drawable.icon_default)
        }
        var fragmentBanner = getFragment<BannerItemFragment>(R.id.fm_banner)
        if (fragmentBanner == null) {
            val bundle = Bundle()
            fragmentBanner = newInstanceFragment(bundle, BannerItemFragment()) as BannerItemFragment
            addChildFragment(R.id.fm_banner, fragmentBanner)
        }

        mViewDataBinding.menuHome.cvMenu1.setOnClickListener {
            val bundle = bundleOf(
                GlobalConstant.Extras.EXTRAS_PATH_VIDEO to R.raw.video_momentum,
                GlobalConstant.Extras.EXTRAS_THEORY_VIDEO to "Momentum dan Impuls",
                GlobalConstant.Extras.EXTRAS_THEORY_DESC to R.drawable.desc1
            )
            startNavigationTo(VideoActivity::class.java, bundle)
        }

        mViewDataBinding.menuHome.cvMenu2.setOnClickListener {
            val bundle = bundleOf(
                GlobalConstant.Extras.EXTRAS_PATH_VIDEO to R.raw.video_kekekalan_momentum,
                GlobalConstant.Extras.EXTRAS_THEORY_VIDEO to "Hukum Kekekalan Momentum",
                GlobalConstant.Extras.EXTRAS_THEORY_DESC to R.drawable.desc2
            )
            startNavigationTo(VideoActivity::class.java, bundle)
        }

        mViewDataBinding.menuHome.cvMenu3.setOnClickListener {
            val bundle = bundleOf(
                GlobalConstant.Extras.EXTRAS_PATH_VIDEO to R.raw.video_aplikasi_kehidupan,
                GlobalConstant.Extras.EXTRAS_THEORY_VIDEO to "Aplikasi dalam Kehidupan",
                GlobalConstant.Extras.EXTRAS_THEORY_DESC to R.drawable.desc3
            )
            startNavigationTo(VideoActivity::class.java, bundle)
        }

        mViewDataBinding.menuHome.cvMenu4.setOnClickListener {
            startNavigationTo(QuizActivity::class.java, Bundle())
        }

        mViewDataBinding.menuHome.cvMenu5.setOnClickListener {
            (activity as MainActivity).infoDialog.setupDialog()
        }
    }

    private fun initListeners() {
        mViewDataBinding.appBar.addOnOffsetChangedListener(object : AppBarStateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout, state: State) {
                when (state) {
                    State.EXPANDED -> {
                        mViewDataBinding.apply {
                            toolbar.visibility = View.GONE
                            tvToolbarTitle.visibility = View.GONE
                        }
                    }
                    State.COLLAPSED -> {
                        mViewDataBinding.apply {
                            toolbar.visibility = View.VISIBLE
                            tvToolbarTitle.visibility = View.VISIBLE
                        }
                    }
                    else -> {
                    }
                }
            }
        })
    }
}