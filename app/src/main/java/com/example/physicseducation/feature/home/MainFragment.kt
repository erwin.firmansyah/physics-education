package com.example.physicseducation.feature.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.physicseducation.MainActivity
import com.example.physicseducation.R
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.databinding.FragmentParentNavigationBinding
import com.example.physicseducation.extension.addChildFragment
import com.example.physicseducation.extension.replaceChildFragment
import com.example.physicseducation.feature.profile.ProfileFragment
import kotlin.system.exitProcess

class MainFragment : BaseFragment<HomeViewModel, FragmentParentNavigationBinding>(){

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentParentNavigationBinding =
        FragmentParentNavigationBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addChildFragment(R.id.fragment_view, HomeFragment())
        mViewDataBinding.bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.navigation_home -> {
                    replaceChildFragment(R.id.fragment_view, HomeFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.navigation_profile -> {
                    replaceChildFragment(R.id.fragment_view, ProfileFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.navigation_exit -> {
                    (activity as MainActivity).customDialog.setupDialog(
                        "Apakah anda ingin keluar dari aplikasi?",
                        true,
                        { (activity as MainActivity).onBackPressed() },
                        R.color.text_primary
                    )
                    return@setOnItemSelectedListener true
                }
            }

            return@setOnItemSelectedListener false
        }

    }
}