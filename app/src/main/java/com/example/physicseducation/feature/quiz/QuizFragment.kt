package com.example.physicseducation.feature.quiz

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.physicseducation.MainActivity
import com.example.physicseducation.R
import com.example.physicseducation.base.BaseFragment
import com.example.physicseducation.databinding.FragmentQuizBinding
import com.example.physicseducation.extension.defaultZero
import com.example.physicseducation.extension.setupHorizontal
import com.example.physicseducation.extension.setupVertical
import com.example.physicseducation.feature.video.VideoViewModel
import com.example.physicseducation.listener.OnAnswerClickListener
import com.example.physicseducation.model.QuizModel
import com.example.physicseducation.util.StaticStorage
import com.google.android.material.snackbar.Snackbar
import java.util.*
import kotlin.concurrent.schedule

class QuizFragment : BaseFragment<VideoViewModel, FragmentQuizBinding>(), OnAnswerClickListener {

    var quizData = arrayListOf<QuizModel.QuestionQuiz>()
    var positionQuiz = 0
    private var navigationAdapter : QuizNavigationAdapter = QuizNavigationAdapter()
    var answerAdapter : QuizAnswerAdapter = QuizAnswerAdapter()
    private var isShowDialog : MutableLiveData<Boolean> = MutableLiveData()

    override fun setViewModel(){}

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentQuizBinding =
        FragmentQuizBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View = mViewDataBinding.containerMain

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        quizData = StaticStorage().getQuizData()
        setupView()
        setListener()
        mViewDataBinding.btnSave.setOnClickListener {
            if(quizData[positionQuiz].answerIsTrue){
                (activity as QuizActivity).trueAnswerLottie.showLottie()
                Timer().schedule(2000) {
                    (activity as QuizActivity).trueAnswerLottie.dismissLottie()
                    isShowDialog.postValue(true)
                }
            }else{
                (activity as QuizActivity).wrongAnswerLottie.showLottie()
                Timer().schedule(2000) {
                    (activity as QuizActivity).wrongAnswerLottie.dismissLottie()
                    isShowDialog.postValue(true)
                }

            }
            setupView()
        }

        mViewDataBinding.ivArrowBack.setOnClickListener {
            (activity as QuizActivity).customDialog.setupDialog(
                "Kuis akan tereset jika anda keluar dari halaman kuis, keluar kuis?",
                true,
                {
                    (activity as QuizActivity).onBackPressed()
                },
                R.color.text_primary
            )
        }
    }

    override fun onClick(answer: QuizModel.Answer) {
        quizData[positionQuiz].userAnswer = answer
        quizData[positionQuiz].answerIsTrue = answer.isTrue
        answerAdapter.updateData(quizData[positionQuiz])
    }

    private fun setupView(){
        navigationAdapter.updateData(quizData)
        navigationAdapter.updatePositionQuizActive(positionQuiz)
        answerAdapter.setListener(this)
        answerAdapter.updateData(quizData[positionQuiz])
        mViewDataBinding.rvQuizNavigation.setupHorizontal(requireContext(), navigationAdapter)
        mViewDataBinding.rvAnswer.setupVertical(requireContext(), answerAdapter)
        mViewDataBinding.tvNoQuestion.text = quizData[positionQuiz].number.toString()
        mViewDataBinding.tvQuestion.text = quizData[positionQuiz].question
    }

    private fun setListener(){
        isShowDialog.observe(viewLifecycleOwner){ dialogShow ->
            if(dialogShow){
                (activity as QuizActivity).reviewDialog.showDialog(
                    quizData[positionQuiz].review,
                    true,
                    onClickYes =
                    {
                        if(positionQuiz == (quizData.size - 1)){
                            showDialogReviewQuiz()
                        }else{
                            positionQuiz += 1
                            setupView()
                            (activity as QuizActivity).reviewDialog.dismissDialog()
                        }
                    }
                )
                isShowDialog.postValue(false)
            }
        }
    }

    private fun showDialogReviewQuiz(){
        var value = 0
        var correctAnswer = 0
        var wrongAnswer = 0

        quizData.forEach { quiz ->
            if(quiz.answerIsTrue){
                correctAnswer++
            }else{
                wrongAnswer++
            }
        }

        value = correctAnswer * 100/quizData.size

        (activity as QuizActivity).reviewQuizDialog.showDialog(
            value = value,
            correctAnswer = correctAnswer,
            wrongAnswer = wrongAnswer,
            onClickYes = {
                startNavigationTo(MainActivity::class.java, Bundle())
            }
        )
    }
}