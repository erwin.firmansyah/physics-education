package com.example.physicseducation

import androidx.fragment.app.Fragment
import com.example.physicseducation.base.BaseFullScreenActivity
import com.example.physicseducation.feature.home.MainFragment
import com.example.physicseducation.util.CustomDialog
import com.example.physicseducation.util.InfoDialog

class MainActivity : BaseFullScreenActivity() {

    val customDialog = CustomDialog(this)
    val infoDialog = InfoDialog(this)

    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        return newInstanceFragment(bundle, MainFragment())
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount >= 1) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}