package com.example.physicseducation.listener

import com.example.physicseducation.model.QuizModel

interface OnAnswerClickListener {
    fun onClick(answer : QuizModel.Answer)
}