package com.example.physicseducation.util

import android.app.Activity
import android.app.Dialog
import com.example.physicseducation.R

class WrongAnswerLottie(private val activity : Activity) {
    private lateinit var isDialog : Dialog
    fun showLottie(){
        isDialog = Dialog(activity)
        isDialog.setContentView(R.layout.wrong_answer)
        isDialog.setCancelable(false)
        isDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        isDialog.show()
    }

    fun dismissLottie(){
        isDialog.dismiss()
    }
}