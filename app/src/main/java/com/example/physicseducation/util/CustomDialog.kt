package com.example.physicseducation.util

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.physicseducation.R

class CustomDialog(private val mActivity: Activity) {
    private lateinit var isDialog : Dialog
    fun setupDialog(message : String, showAction: Boolean = true, onClickYes: () -> Unit, textColor: Int){

        isDialog = Dialog(mActivity)
        isDialog.setCancelable(true)
        isDialog.setContentView(R.layout.layout_custom_dialog)
        isDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val tvMsg = isDialog.findViewById(R.id.tvMsg) as TextView
        tvMsg.text = message
        tvMsg.setTextColor(ContextCompat.getColor(mActivity.baseContext, textColor))

        val linearLayoutAction = isDialog.findViewById(R.id.ll_action) as LinearLayout
        linearLayoutAction.visibility = if(showAction) View.VISIBLE else View.GONE

        val btnYes = isDialog.findViewById(R.id.btn_yes) as Button
        val btnNo = isDialog.findViewById(R.id.btn_no) as Button

        btnYes.setOnClickListener {
            dismissDialog()
            onClickYes()
        }

        btnNo.setOnClickListener { isDialog.dismiss() }
        isDialog.show()
    }

    fun dismissDialog(){
        isDialog.dismiss()
    }
}