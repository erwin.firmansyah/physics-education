package com.example.physicseducation.util

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.physicseducation.R

class ReviewQuizDialog(private val mActivity: Activity) {
    private lateinit var isDialog : Dialog
    fun showDialog(value : Int, correctAnswer: Int, wrongAnswer: Int, showAction: Boolean = true, onClickYes: () -> Unit){

        isDialog = Dialog(mActivity)
        isDialog.setCancelable(true)
        isDialog.setContentView(R.layout.layout_result_quiz_dialog)
        isDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val tvCorrectAnswer = isDialog.findViewById(R.id.tv_correct_answer) as TextView
        val tvWrongAnswer = isDialog.findViewById(R.id.tv_wrong_answer) as TextView
        val tvValue = isDialog.findViewById(R.id.tv_value) as TextView
        val correctAnswerText = "Jawaban Benar : $correctAnswer"
        tvCorrectAnswer.text = correctAnswerText
        val wrongAnswerText = "Jawaban Salah : $wrongAnswer"
        tvWrongAnswer.text = wrongAnswerText
        val valueText = "$value/100"
        tvValue.text = valueText

        val linearLayoutAction = isDialog.findViewById(R.id.ll_action) as LinearLayout
        linearLayoutAction.visibility = if(showAction) View.VISIBLE else View.GONE

        val btnFinish = isDialog.findViewById(R.id.btn_finish) as Button

        btnFinish.setOnClickListener {
            dismissDialog()
            onClickYes()
        }

        isDialog.show()
    }

    fun dismissDialog(){
        isDialog.dismiss()
    }
}