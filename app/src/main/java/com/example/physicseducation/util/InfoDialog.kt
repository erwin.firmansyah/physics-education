package com.example.physicseducation.util

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.physicseducation.R

class InfoDialog(private val mActivity: Activity) {
    private lateinit var isDialog : Dialog
    fun setupDialog(){
        isDialog = Dialog(mActivity)
        isDialog.setCancelable(true)
        isDialog.setContentView(R.layout.layout_info)
        isDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        isDialog.show()
    }
}