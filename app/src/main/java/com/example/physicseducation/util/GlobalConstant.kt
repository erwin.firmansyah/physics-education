package com.example.physicseducation.util

class GlobalConstant {

    companion object {
        const val EMPTY_STRING = ""
        const val TOOLBAR_TITLE = "toolbar_title"
        const val TOOLBAR_BACKGROUND_COLOR = "toolbar_background_color"
        const val LOCALE_ID = "id"
    }

    object Extras {
        const val EXTRAS_NEW = "new"
        const val EXTRAS_NAME = "name"
        const val EXTRAS_PROFILE_URL = "profile_image"
        const val EXTRAS_PATH_VIDEO = "video"
        const val EXTRAS_THEORY_VIDEO = "theory"
        const val EXTRAS_THEORY_DESC = "desc"

    }
}