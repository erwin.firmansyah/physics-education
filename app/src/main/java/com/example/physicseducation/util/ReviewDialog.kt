package com.example.physicseducation.util

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.physicseducation.R

class ReviewDialog(private val mActivity: Activity) {
    private lateinit var isDialog : Dialog
    fun showDialog(reviewImg : Int, showAction: Boolean = true, onClickYes: () -> Unit){

        isDialog = Dialog(mActivity)
        isDialog.setCancelable(true)
        isDialog.setContentView(R.layout.layout_review_dialog)
        isDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val ivReview = isDialog.findViewById(R.id.iv_review) as ImageView
        ivReview.setImageResource(reviewImg)

        val linearLayoutAction = isDialog.findViewById(R.id.ll_action) as LinearLayout
        linearLayoutAction.visibility = if(showAction) View.VISIBLE else View.GONE

        val btnNext = isDialog.findViewById(R.id.btn_next) as Button

        btnNext.setOnClickListener {
            dismissDialog()
            onClickYes()
        }

        isDialog.show()
    }

    fun dismissDialog(){
        isDialog.dismiss()
    }
}