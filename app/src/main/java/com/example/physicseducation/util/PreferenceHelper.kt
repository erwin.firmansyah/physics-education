package com.example.physicseducation.util

import android.content.Context
import android.content.SharedPreferences
import com.example.physicseducation.extension.defaultEmpty

class PreferenceHelper(context : Context) {

    private val pref = "PhysicsEducation"
    private var sharedPref : SharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE)
    private var editor : SharedPreferences.Editor = sharedPref.edit()

    init{
        editor.apply()
    }

    fun setProfileName(name : String){
        editor.putString(GlobalConstant.Extras.EXTRAS_NAME, name)
        editor.commit()
    }

    fun getProfileName(): String {
        return sharedPref.getString(GlobalConstant.Extras.EXTRAS_NAME, "").defaultEmpty()
    }

    fun saveImageProfileUri(name : String){
        editor.putString(GlobalConstant.Extras.EXTRAS_PROFILE_URL, name)
        editor.commit()
    }

    fun getImageProfileUri(): String {
        return sharedPref.getString(GlobalConstant.Extras.EXTRAS_PROFILE_URL, "").defaultEmpty()
    }

    fun setFirstInstall(state : Boolean){
        editor.putBoolean(GlobalConstant.Extras.EXTRAS_NEW, state)
        editor.commit()
    }

    fun isFirstInstall(): Boolean {
        return sharedPref.getBoolean(GlobalConstant.Extras.EXTRAS_NEW, true)
    }

}