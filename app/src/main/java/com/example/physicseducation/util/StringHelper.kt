package com.example.physicseducation.util

import android.text.TextUtils
import com.example.physicseducation.util.GlobalConstant.Companion.LOCALE_ID
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

object StringHelper {

    private const val SECOND_MILLIS = 1000
    private const val MINUTE_MILLIS = 60 * SECOND_MILLIS
    private const val HOUR_MILLIS = 60 * MINUTE_MILLIS
    private const val DAY_MILLIS = 24 * HOUR_MILLIS
    const val DEFAULT_PATTERN_TIME_APP = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val DEFAULT_PATTERN_TIME = "yyyy-MM-dd HH:mm"
    const val DEFAULT_PATTERN_DATE = "dd-MM-yyyy"

    fun convertToCurrencyFormat(price: String, shouldAddCurrency: Boolean): String {
        val locale = Locale("in", LOCALE_ID)
        val symbols = DecimalFormatSymbols.getInstance(locale)
        symbols.groupingSeparator = '.'
        symbols.monetaryDecimalSeparator = ','

        if (shouldAddCurrency) {
            symbols.currencySymbol = symbols.currencySymbol
        } else {
            symbols.currencySymbol = ""
        }

        val df = DecimalFormat.getCurrencyInstance(locale) as DecimalFormat
        val kursIndonesia = DecimalFormat(df.toPattern(), symbols)
        kursIndonesia.maximumFractionDigits = 0

        return if (TextUtils.isEmpty(price)) {
            kursIndonesia.format(0)
        } else {
            kursIndonesia.format(java.lang.Double.parseDouble(price))
        }
    }

    fun getCurrentTime(): String {
        val dateFormat = SimpleDateFormat(DEFAULT_PATTERN_TIME)
        dateFormat.timeZone = TimeZone.getTimeZone("GMT+7")
        val today = Calendar.getInstance().time
        return dateFormat.format(today)
    }

    fun getDate(): String {
        val dateFormat = SimpleDateFormat(DEFAULT_PATTERN_DATE)
        dateFormat.timeZone = TimeZone.getTimeZone("GMT+7")
        val today = Calendar.getInstance().time
        return dateFormat.format(today)
    }
}