package com.example.physicseducation.util

import com.example.physicseducation.R
import com.example.physicseducation.model.QuizModel

class StaticStorage {

    fun getQuizData(): ArrayList<QuizModel.QuestionQuiz> {

        return arrayListOf(
            QuizModel.QuestionQuiz(
                number = 1,
                question = "Sebuah mobil dengan massa 2000 kg, mula-mula bergerak lurus dengan kecepatan awal 20 m/s ke utara. Setelah beberapa saat, mobil tersebut direm dan setelah 10 detik kecepatannya berkurang menjadi 5 m/s. Berapakah perubahan momentumnya setelah di rem? (arah utara sebagai arah positif)",
                review = R.drawable.review_1,
                answerIsTrue = false,
                answerList = listOf(
                    QuizModel.Answer(
                        position = "A",
                        answerText = "30.000 kg m/s, ke arah utara",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "B",
                        answerText = "30.000 kg m/s, ke arah selatan",
                        isTrue = true
                    ),
                    QuizModel.Answer(
                        position = "C",
                        answerText = "40.000 kg m/s, ke arah utara",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "D",
                        answerText = "50.000 kg m/s, ke arah selatan",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "E",
                        answerText = "50.000 kg m/s, ke arah utara",
                        isTrue = false
                    )
                )
            ),
            QuizModel.QuestionQuiz(
                number = 2,
                question = "Sebuah bola basket jatuh dari ketinggian H dan memantul setinggi 1/4H setelah mengenai lantai. Manakah pernyataan yang benar di bawah ini?",
                review = R.drawable.review_2,
                answerIsTrue = false,
                answerList = listOf(
                    QuizModel.Answer(
                        position = "A",
                        answerText = "Peristiwa ini termasuk tidak lenting sama sekali karena seluruh energi kinetiknya habis",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "B",
                        answerText = "Peristiwa ini termasuk lenting sebagian karena energi kinetic berubah",
                        isTrue = true
                    ),
                    QuizModel.Answer(
                        position = "C",
                        answerText = "Peristiwa ini termasuk lenting Sebagian karena energi kinetic tidak berubah",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "D",
                        answerText = "Peristiwa ini termasuk lenting sempurna karena energi kinetic berubah",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "E",
                        answerText = "Peristiwa ini termasuk lenting sempurna karena energi kinetic tidak berubah",
                        isTrue = false
                    )
                )
            ),
            QuizModel.QuestionQuiz(
                number = 3,
                question = "Sebuah mobil menabrak dinding dengan waktu kontak 1,1s. jika resultan gaya 3.250 N saat terjadi tabrakan. Maka berapa besar perubahan momentum yang terjadi pada mobil?",
                review = R.drawable.review_3,
                answerIsTrue = false,
                answerList = listOf(
                    QuizModel.Answer(
                        position = "A",
                        answerText = "1.865 kg m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "B",
                        answerText = "2.725 kg m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "C",
                        answerText = "3.575 kg m/s",
                        isTrue = true
                    ),
                    QuizModel.Answer(
                        position = "D",
                        answerText = "4.835 kg m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "E",
                        answerText = "5.875 kg m/s",
                        isTrue = false
                    )
                )
            ),
            QuizModel.QuestionQuiz(
                number = 4,
                question = "Sebuah peluru 7,5 g bergerak 350 m/s ditembakkan ke balok kayu 375 g yang diam di atas permukaan licin. Peluru menembus balok kayu namun dengan kecepatan yang lebih rendah yaitu 200 m/s. kecepatan balok setelah ditembus peluru adalah…",
                review = R.drawable.review_4,
                answerIsTrue = false,
                answerList = listOf(
                    QuizModel.Answer(
                        position = "A",
                        answerText = "1 m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "B",
                        answerText = "2 m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "C",
                        answerText = "3 m/s",
                        isTrue = true
                    ),
                    QuizModel.Answer(
                        position = "D",
                        answerText = "4 m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "E",
                        answerText = "5 m/s",
                        isTrue = false
                    )
                )
            ),
            QuizModel.QuestionQuiz(
                number = 5,
                question = "Benda A bergerak ke arah kanan dengan kecepatan 5 m/s, sedangkan benda B bergerak ke kiri dengan kecepatan 3 m/s. Jika masa kedua benda sama dan terjadi tumbukan lenting sempurna, maka berapakah kecapatan benda A setelah tumbukan?",
                review = R.drawable.review_5,
                answerIsTrue = false,
                answerList = listOf(
                    QuizModel.Answer(
                        position = "A",
                        answerText = "3 m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "B",
                        answerText = "-3 m/s",
                        isTrue = true
                    ),
                    QuizModel.Answer(
                        position = "C",
                        answerText = "5 m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "D",
                        answerText = "-5 m/s",
                        isTrue = false
                    ),
                    QuizModel.Answer(
                        position = "E",
                        answerText = "0 m/s",
                        isTrue = false
                    )
                )
            ),
        )
    }
}