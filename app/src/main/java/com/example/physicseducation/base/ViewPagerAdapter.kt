package com.example.physicseducation.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter


class ViewPagerAdapter(fa: FragmentManager, lifecycle: Lifecycle, private var fragments: ArrayList<Fragment>) : FragmentStateAdapter(fa, lifecycle) {

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment = fragments[position]

    fun getFragment(position: Int): Fragment = fragments[position]

    fun updatePager(fragments: List<Fragment>) {
        this.fragments = ArrayList(fragments)
        notifyDataSetChanged()
    }
}